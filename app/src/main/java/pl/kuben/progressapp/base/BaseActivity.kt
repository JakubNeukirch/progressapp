package pl.kuben.progressapp.base

import android.arch.lifecycle.Observer
import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

abstract class BaseActivity<T : BaseViewModel> : AppCompatActivity() {

    abstract val viewModel: T

    open protected val snackBarRoot: View by lazy { window.decorView.findViewById(android.R.id.content) as View }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.showMessage.observe(this, Observer {
            if (it != null) {
                showSnackBar(it)
                viewModel.showMessage.value = null
            }
        })
    }

    override fun onDestroy() {
        viewModel.onDestroy()
        super.onDestroy()
    }

    fun showSnackBar(message: String) {
        Snackbar.make(
                snackBarRoot,
                message,
                Snackbar.LENGTH_LONG
        )
                .show()
    }

    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }
}