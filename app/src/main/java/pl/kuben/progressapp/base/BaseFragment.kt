package pl.kuben.progressapp.base

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.flask.colorpicker.ColorPickerView
import com.flask.colorpicker.builder.ColorPickerDialogBuilder
import pl.kuben.progressapp.R

abstract class BaseFragment<T : BaseViewModel> : Fragment() {
    abstract val viewModel: T

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.showMessage.observe(this, Observer {
            if (it != null) {
                showSnackBar(it)
                viewModel.showMessage.value = null
            }
        })
    }

    protected fun openColorPicker(color: Int, onApproved: (color: Int) -> Unit) {
        ColorPickerDialogBuilder
                .with(activity!!)
                .setTitle(getString(R.string.choose_color))
                .initialColor(color)
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setPositiveButton(getString(R.string.ok)) { dialog, selected, _ ->
                    onApproved(selected)
                    dialog.dismiss()
                }
                .build()
                .show()
    }

    fun showSnackBar(text: String) {
        (activity as BaseActivity<*>?)?.showSnackBar(text)
    }
}