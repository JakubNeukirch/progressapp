package pl.kuben.progressapp.base

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.support.annotation.ColorInt
import android.support.annotation.ColorRes
import io.reactivex.disposables.CompositeDisposable
import pl.kuben.progressapp.data.ResourceProvider

open class BaseViewModel(private val resourceProvider: ResourceProvider) : ViewModel() {
    protected var disposables = CompositeDisposable()
    val showMessage = MutableLiveData<String>()
    var message: String
        get() = showMessage.value ?: ""
        set(value) {
            showMessage.value = value
        }

    fun onDestroy() {
        disposables.dispose()
    }

    protected fun getString(stringId: Int): String {
        return resourceProvider.getString(stringId)
    }

    protected fun getString(stringId: Int, vararg args: Any): String {
        return resourceProvider.getString(stringId, *args)
    }

    @ColorInt
    protected fun getColor(@ColorRes colorId: Int): Int {
        return resourceProvider.getColor(colorId)
    }
}