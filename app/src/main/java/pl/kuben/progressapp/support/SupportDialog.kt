package pl.kuben.progressapp.support

import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.ViewGroup
import kotlinx.android.synthetic.main.dialog_support.*
import pl.kuben.progressapp.R
import pl.kuben.progressapp.common.dismissAfter
import pl.kuben.progressapp.common.openUrlIntent
import pl.kuben.progressapp.common.setOnMultipleClickListener

class SupportDialog(context: Context) : AlertDialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        )
        window?.setBackgroundDrawable(null)
        setContentView(R.layout.dialog_support)
        setCancelable(true)
        setupDialog()
    }

    private fun setupDialog() {
        aboutButton.setOnClickListener {
            dismiss()
        }
        twitterDonateImageView.setOnClickListener {
            if (!context.openUrlIntent(context.getString(R.string.twitter_url))) {
                showSnackBar(context.getString(R.string.no_activity_intent))
            }
        }
        payPalDonateImageView.setOnClickListener {
            if (!context.openUrlIntent(context.getString(R.string.paypal_url))) {
                showSnackBar(context.getString(R.string.no_activity_intent))
            }
        }
        moneroDonateImageView.setOnClickListener {
            with(context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager) {
                this.primaryClip = ClipData.newPlainText(
                        "MoneroAddress",
                        "48WYZUnrRTpgRP7tWVfU1STGGhdef1megW1YvFzFycUpMxQEdhuKu2FjS8e5hS39w9JqPoLYHh5TLVh7Buc4jwz6UdCDnKZ"
                )
                showSnackBar(context.getString(R.string.monero_copied))
            }
        }
        coordinatorLayout.setOnClickListener { dismiss() }
        dialogView.setOnMultipleClickListener(4) {
            AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(R.layout.dialog_launch)
                    .create()
                    .dismissAfter(1000)
                    .show()
        }
    }

    private fun showSnackBar(text: String) {
        Snackbar.make(coordinatorLayout, text, Snackbar.LENGTH_LONG).show()
    }
}