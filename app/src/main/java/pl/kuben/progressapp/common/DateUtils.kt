package pl.kuben.progressapp.common

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
private val dateFormat = SimpleDateFormat("dd.MM")

private const val DAY_IN_MILLIS = 1000 * 60 * 60 * 24

fun Long.toDateString(): String {
    val date = Date()
    date.time = this
    return dateFormat.format(date)
}

fun Long.isToday(): Boolean {
    val today = GregorianCalendar.getInstance()
            .apply {
                set(GregorianCalendar.HOUR_OF_DAY, 0)
                set(GregorianCalendar.MINUTE, 0)
                set(GregorianCalendar.SECOND, 0)
                set(GregorianCalendar.MILLISECOND, 0)
            }
    return this >= today.timeInMillis
}

fun Long.daysBefore(): Int {
    val difference = System.currentTimeMillis() - this
    return (difference / DAY_IN_MILLIS).toInt()
}