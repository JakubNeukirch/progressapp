package pl.kuben.progressapp.common

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.MotionEvent
import android.view.View
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

fun Context.openUrlIntent(url: String): Boolean {
    val intent = Intent(Intent.ACTION_VIEW)
    return if (intent.resolveActivity(packageManager) != null) {
        intent.data = Uri.parse(url)
        startActivity(intent)
        true
    } else {
        false
    }
}

class MultipleClickListener(val expectedClicks: Int, val action: () -> Unit) : View.OnTouchListener {

    var maxDelay = 300
    private var lastClick: Long = 0
    private var clickCount = 0

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_UP) {
            val difference = System.currentTimeMillis() - lastClick
            if (difference <= maxDelay) {
                clickCount++
            } else {
                clickCount = 1
            }
            lastClick = System.currentTimeMillis()
            if (clickCount == expectedClicks) {
                action()
            }
        }
        return true
    }
}

fun View.setOnMultipleClickListener(expectedClicks: Int = 2, action: () -> Unit) {
    this.setOnTouchListener(MultipleClickListener(expectedClicks, action))
}

fun AlertDialog.dismissAfter(millis: Long): AlertDialog {
    val disposable = Observable.timer(millis, TimeUnit.MILLISECONDS)
            .subscribeBy {
                this.dismiss()
            }
    this.setOnDismissListener {
        disposable.dispose()
    }
    return this
}