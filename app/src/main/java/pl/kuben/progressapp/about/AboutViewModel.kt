package pl.kuben.progressapp.about

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import pl.kuben.progressapp.base.BaseViewModel
import pl.kuben.progressapp.data.ContributorsRepository
import pl.kuben.progressapp.data.ResourceProvider
import pl.kuben.progressapp.data.model.Contributor

class AboutViewModel(resourceProvider: ResourceProvider) : BaseViewModel(resourceProvider) {

    private val _contributors = MutableLiveData<List<Contributor>>()
    val contributors: LiveData<List<Contributor>> by lazy { _contributors }

    fun loadContributors() {
        _contributors.value = ContributorsRepository.contributors
    }
}