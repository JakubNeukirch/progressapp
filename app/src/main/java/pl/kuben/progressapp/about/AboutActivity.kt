package pl.kuben.progressapp.about

import android.app.Dialog
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_about.*
import org.koin.android.viewmodel.ext.android.viewModel
import pl.kuben.progressapp.R
import pl.kuben.progressapp.base.BaseActivity
import pl.kuben.progressapp.common.openUrlIntent
import pl.kuben.progressapp.support.SupportDialog

class AboutActivity : BaseActivity<AboutViewModel>() {

    override val viewModel: AboutViewModel by viewModel()
    private val adapter = ContributorsAdapter()
    private val supportDialog: Dialog by lazy { SupportDialog(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        setupScreen()
    }

    private fun setupScreen() {
        setupRecyclerView()
        subscribeToContributors()
        setSupportActionBar(aboutAppBar)
        viewModel.loadContributors()
        aboutAppBar.setNavigationOnClickListener { onBackPressed() }
    }

    private fun setupRecyclerView() {
        adapter.onPageClick = {
            if (!openUrlIntent(it)) {
                showSnackBar(getString(R.string.no_activity_intent))
            }
        }
        aboutRecyclerView.adapter = adapter
        aboutRecyclerView.layoutManager = LinearLayoutManager(this)
    }

    private fun subscribeToContributors() {
        viewModel.contributors.observe(this, Observer {
            adapter.contributors = it ?: listOf()
        })
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.donateItem) {
            supportDialog.show()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_about, menu)
        return true
    }
}
