package pl.kuben.progressapp.drawables

import android.content.res.Resources
import android.graphics.*

open class GraphicDrawable(
        resources: Resources,
        style: Int
) : BaseDrawable(resources, style) {

    var progressBitmap: Bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)
    private val shaderMatrix = Matrix()
    var color: Int = Color.RED

    override fun draw(canvas: Canvas) {
        invalidate()
        canvas.drawColor(progressBackgroundColor, PorterDuff.Mode.CLEAR)
        canvas.drawColor(progressBackgroundColor)
        canvas.drawRect(rect, paint)
    }

    override fun invalidate() {
        invalidateBitmapSize()
        invalidatePaint()
    }

    private fun invalidateBitmapSize() {
        if (rect.height() > 0 && rect.width() > 0) {
            val ratio: Float = progressBitmap.width / progressBitmap.height.toFloat()
            progressBitmap = Bitmap.createScaledBitmap(
                    progressBitmap,
                    Math.round(rect.height() * ratio),
                    rect.height(),
                    false)
        }
    }

    private fun invalidatePaint() {
        shaderMatrix.setTranslate(rect.left.toFloat(), rect.top.toFloat())
        paint.shader = BitmapShader(progressBitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT)
        paint.shader.setLocalMatrix(shaderMatrix)
    }

}