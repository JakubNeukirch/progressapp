package pl.kuben.progressapp.data.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import io.reactivex.Single
import pl.kuben.progressapp.data.model.db.WebProgress

@Dao
interface WebProgressDao {

    @Insert
    fun addWebProgress(webProgress: WebProgress): Long

    @Query("DELETE FROM WebProgress WHERE id = :webProgressId")
    fun deleteWebProgress(webProgressId: Long): Int

    @Query("SELECT * FROM WebProgress wp JOIN WebEntity we ON wp.id=we.web_progress_id WHERE we.progress_id = :progressId")
    fun getWithProgress(progressId: Int): Single<List<WebProgress>>

    @Query("DELETE FROM WebProgress WHERE id IN (:progressIds)")
    fun deleteWebProgresses(progressIds: List<Long>): Int
}