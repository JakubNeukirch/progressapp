package pl.kuben.progressapp.data.model

import pl.kuben.progressapp.data.model.db.ProgressCount

data class WebProgressEntity(
        val id: Long,
        val name: String,
        val color: Int,
        var progresses: List<ProgressCount>? = null
)