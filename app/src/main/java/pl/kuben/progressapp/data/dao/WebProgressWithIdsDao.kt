package pl.kuben.progressapp.data.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import io.reactivex.Single
import pl.kuben.progressapp.data.model.db.WebProgressWithIds

@Dao
interface WebProgressWithIdsDao {

    /*@Query("SELECT * FROM WebProgress wp JOIN WebEntity we on we.web_progress_id=wp.id")
   fun getAll() : List<WebProgressWithIds>*/

    @Query("SELECT wp.id as web_progress_id, wp.name as name, wp.color as color, we.progress_id as progresses FROM WebProgress wp " +
            "JOIN WebEntity we on we.web_progress_id=wp.id " +
            "WHERE wp.id = :webProgressId " +
            "GROUP BY wp.id, wp.name, wp.color")
    fun get(webProgressId: Long): Single<WebProgressWithIds>

    @Query("SELECT wp.id as web_progress_id, wp.name as name, wp.color as color, we.progress_id as progresses FROM WebProgress wp " +
            "JOIN WebEntity we on we.web_progress_id=wp.id " +
            "GROUP BY wp.id, wp.name, wp.color")
    fun getAll(): Single<List<WebProgressWithIds>>

}