package pl.kuben.progressapp.data.model.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.PrimaryKey

data class ProgressCount(
        val name: String,
        val maxValue: Int,
        val color: Int,
        val count: Int,
        @ColumnInfo(name = "background_color") val backgroundColor: Int,
        val style: Int,
        val timestamp: Long
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
}