package pl.kuben.progressapp.data

import io.reactivex.Completable
import io.reactivex.Single
import pl.kuben.progressapp.data.dao.*
import pl.kuben.progressapp.data.model.WebProgressEntity
import pl.kuben.progressapp.data.model.db.*

class Repository constructor(
        private val progressDao: ProgressDao,
        private val progressCountDao: ProgressCountDao,
        private val entryDao: EntryDao,
        private val webProgressDao: WebProgressDao,
        private val webProgressWithIdsDao: WebProgressWithIdsDao,
        private val webEntityDao: WebEntityDao
) {
    fun insertProgress(progress: Progress): Single<Long> {
        return Single.create { it.onSuccess(progressDao.insert(progress)) }
    }

    fun insertEntry(entry: Entry): Single<Long> {
        return Single.create {
            it.onSuccess(entryDao.insert(entry))
        }
    }

    fun insertWebProgress(webProgress: WebProgress): Single<Long> {
        return Single.create {
            it.onSuccess(webProgressDao.addWebProgress(webProgress))
        }
    }

    fun insertWebEntities(webEntities: List<WebEntity>): Single<List<Long>> {
        return Single.create {
            it.onSuccess(webEntityDao.addWebEntities(webEntities))
        }
    }

    fun getProgressWithIds(webProgressId: Long): Single<WebProgressWithIds> {
        return webProgressWithIdsDao.get(webProgressId)
    }

    fun deleteEntry(id: Long): Single<Int> {
        return Single.create {
            it.onSuccess(entryDao.deleteEntry(id))
        }
    }

    fun deleteProgressWebEntities(progressId: Int): Single<Int> {
        return singleOf { webEntityDao.deleteProgressWebEntities(progressId) }
    }

    fun getEntries(progressId: Int): Single<List<Entry>> {
        return entryDao.getEntries(progressId)
    }

    fun getProgressCounts(): Single<List<ProgressCount>> = progressCountDao.getAll()

    fun getProgress(progressId: Int) = progressDao.getProgress(progressId)

    fun deleteProgress(progressId: Int): Completable {
        return Completable.fromAction {
            progressDao.deleteProgress(progressId)
            entryDao.deleteEntries(progressId)
        }
    }

    fun getWebProgresses(): Single<List<WebProgressEntity>> {
        return webProgressWithIdsDao.getAll()
                .flatMap { list ->
                    val listSingles = list.map {
                        progressCountDao.getProgressCounts(it.progresses ?: listOf())
                    }

                    var counter = 0
                    return@flatMap Single.concat(listSingles)
                            .map {
                                val item = list[counter++]
                                return@map WebProgressEntity(
                                        item.id,
                                        item.name,
                                        item.color,
                                        it
                                )
                            }
                            .toList()
                }
    }

    fun deleteWebProgressesWithProgress(progressId: Int): Single<Int> {
        return webProgressDao.getWithProgress(progressId)
                .flatMap {
                    return@flatMap deleteWebProgresses(it)
                }
    }

    fun deleteWebProgresses(webProgresses: List<WebProgress>): Single<Int> {
        return singleOf {
            webProgressDao.deleteWebProgresses(
                    webProgresses.map { webProgress -> webProgress.id!! }
            )
        }
    }

    fun getWebProgress(webProgressId: Long): Single<WebProgressEntity> {
        return webProgressWithIdsDao.get(webProgressId)
                .flatMap { webProgressWithIds ->
                    return@flatMap progressCountDao.getProgressCounts(webProgressWithIds.progresses
                            ?: listOf())
                            .map {
                                return@map WebProgressEntity(
                                        webProgressWithIds.id,
                                        webProgressWithIds.name,
                                        webProgressWithIds.color,
                                        it
                                )
                            }
                }
    }

    fun deleteWebProgress(webProgressId: Long): Single<Int> {
        return Single.create {
            try {
                it.onSuccess(webProgressDao.deleteWebProgress(webProgressId))
            } catch (ex: Exception) {
                it.onError(ex)
            }
        }
    }

    private fun <T> singleOf(action: () -> T): Single<T> {
        return Single.create {
            try {
                it.onSuccess(action())
            } catch (ex: Exception) {
                it.onError(ex)
            }
        }
    }
}