package pl.kuben.progressapp.data.model.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@ForeignKey(
        entity = ProgressCount::class,
        parentColumns = ["id"],
        childColumns = ["progressId"]
)
@Entity(tableName = "entry")
data class Entry(
        val count: Int,
        val date: Long,
        val progressId: Int
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
}