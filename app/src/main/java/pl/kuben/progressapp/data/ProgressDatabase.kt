package pl.kuben.progressapp.data

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.migration.Migration
import pl.kuben.progressapp.data.dao.*
import pl.kuben.progressapp.data.model.db.Entry
import pl.kuben.progressapp.data.model.db.Progress
import pl.kuben.progressapp.data.model.db.WebEntity
import pl.kuben.progressapp.data.model.db.WebProgress

@Database(entities = [Entry::class, Progress::class, WebEntity::class, WebProgress::class], version = 4)
abstract class ProgressDatabase : RoomDatabase() {
    abstract fun progressDao(): ProgressDao
    abstract fun entryDao(): EntryDao
    abstract fun progressCountDao(): ProgressCountDao
    abstract fun webEntityDao(): WebEntityDao
    abstract fun webProgressDao(): WebProgressDao
    abstract fun webProgressWithIdsDao(): WebProgressWithIdsDao
}