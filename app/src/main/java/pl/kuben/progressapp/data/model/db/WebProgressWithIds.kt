package pl.kuben.progressapp.data.model.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Relation

data class WebProgressWithIds(
        @ColumnInfo(name = "web_progress_id")
        val id: Long,
        @ColumnInfo(name = "name")
        val name: String,
        @ColumnInfo(name = "color")
        val color: Int
) {

    @Relation(parentColumn = "web_progress_id", entityColumn = "web_progress_id", entity = WebEntity::class, projection = ["progress_id"])
    var progresses: List<Long>? = null
}