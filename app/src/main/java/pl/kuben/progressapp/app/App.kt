package pl.kuben.progressapp.app

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import org.koin.android.ext.android.startKoin
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import pl.kuben.progressapp.R
import pl.kuben.progressapp.about.AboutViewModel
import pl.kuben.progressapp.add.AddViewModel
import pl.kuben.progressapp.add.progress.AddProgressViewModel
import pl.kuben.progressapp.add.web.AddWebViewModel
import pl.kuben.progressapp.common.Settings
import pl.kuben.progressapp.data.AndroidResourceProvider
import pl.kuben.progressapp.data.ProgressDatabase
import pl.kuben.progressapp.data.Repository
import pl.kuben.progressapp.data.ResourceProvider
import pl.kuben.progressapp.details.DetailsViewModel
import pl.kuben.progressapp.main.MainViewModel
import pl.kuben.progressapp.plain.PlainViewModel
import pl.kuben.progressapp.splash.SplashViewModel
import pl.kuben.progressapp.web.WebViewModel
import pl.kuben.progressapp.webParent.WebParentViewModel
import timber.log.Timber
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

class App : Application() {

    private val appModule = module {
        viewModel { MainViewModel(get(), get()) }
        viewModel { AddProgressViewModel(get(), get()) }
        viewModel { AddWebViewModel(get(), get()) }
        viewModel { AddViewModel(get()) }
        viewModel { DetailsViewModel(get(), get()) }
        viewModel { PlainViewModel(get(), get(), get()) }
        viewModel { WebViewModel(get(), get()) }
        viewModel { WebParentViewModel(get(), get(), get()) }
        viewModel { SplashViewModel(get()) }
        viewModel { AboutViewModel(get()) }

        single { getSharedPreferences("progress", Context.MODE_PRIVATE) }
        single { Settings(get()) }
        single {
            val db: ProgressDatabase = get()
            Repository(
                    db.progressDao(),
                    db.progressCountDao(),
                    db.entryDao(),
                    db.webProgressDao(),
                    db.webProgressWithIdsDao(),
                    db.webEntityDao()
            )
        }
        single {
            Room.databaseBuilder(this@App.applicationContext, ProgressDatabase::class.java, "progress-db")
                    .fallbackToDestructiveMigration()
                    .build()
        }
        single { resources }
        single { AndroidResourceProvider(get()) as ResourceProvider }
    }

    override fun onCreate() {
        super.onCreate()
        setupFont()
        Timber.plant(Timber.DebugTree())
        startKoin(this, listOf(appModule))
    }

    private fun setupFont() {
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Quicksand-Medium.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build())
    }
}