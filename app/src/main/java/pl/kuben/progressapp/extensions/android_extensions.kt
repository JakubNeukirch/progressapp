package pl.kuben.progressapp.extensions

import android.app.Activity
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat

fun Activity.getColorInt(@ColorRes colorId: Int): Int {
    return ContextCompat.getColor(this, colorId)
}