package pl.kuben.progressapp.main

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.layout_add.view.*
import kotlinx.android.synthetic.main.layout_options.view.*
import kotlinx.android.synthetic.main.recycler_item_progress.view.*
import pl.kuben.progressapp.R
import pl.kuben.progressapp.data.model.db.ProgressCount

class ProgressAdapter : RecyclerView.Adapter<ProgressAdapter.ViewHolder>() {

    var list: List<ProgressCount> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var onAddClick: (progress: ProgressCount) -> Unit = {}
    var onMinusClick: (progress: ProgressCount) -> Unit = {}
    var onEditClick: (progress: ProgressCount) -> Unit = {}
    var onDetailsClick: (progressId: Int) -> Unit = {}
    var onDeleteClick: (progress: ProgressCount) -> Unit = {}
    var onItemClick: (progress: ProgressCount) -> Unit = {}

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        init {
            with(itemView) {
                addProgressButton.setOnClickListener { onAddClick(list[adapterPosition]) }
                minusProgressButton.setOnClickListener { onMinusClick(list[adapterPosition]) }
                addProgressButton.setOnClickListener { onAddClick(list[adapterPosition]) }
                editButton.setOnClickListener { onEditClick(list[adapterPosition]) }
                deleteButton.setOnClickListener { onDeleteClick(list[adapterPosition]) }
                detailsButton.setOnClickListener { onDetailsClick(list[adapterPosition].id!!) }
                setOnClickListener { onItemClick(list[adapterPosition]) }
            }
        }

        fun bind(progress: ProgressCount, position: Int) {
            with(itemView) {
                nameTextView.text = progress.name
                itemProgressBar.setProgress(progress)
                countTextView.text = context.getString(R.string.progress_count, progress.count, progress.maxValue)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycler_item_progress, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position >= itemCount - 1) {
            setBottomMargin(holder.itemView, R.dimen.main_padding_bottom)
        } else {
            setBottomMargin(holder.itemView)
        }
        holder.bind(list[position], position)
    }

    private fun setBottomMargin(view: View, dimen: Int? = null) {
        if (dimen != null) {
            (view.layoutParams as RecyclerView.LayoutParams).bottomMargin =
                    view.resources.getDimensionPixelSize(dimen)
        } else {
            (view.layoutParams as RecyclerView.LayoutParams).bottomMargin = 0
        }
    }
}