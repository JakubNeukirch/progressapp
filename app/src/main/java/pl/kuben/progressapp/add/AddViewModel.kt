package pl.kuben.progressapp.add

import pl.kuben.progressapp.base.BaseViewModel
import pl.kuben.progressapp.data.ResourceProvider

class AddViewModel(resourceProvider: ResourceProvider) : BaseViewModel(resourceProvider)