package pl.kuben.progressapp.add.progress

import android.arch.lifecycle.Observer
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import kotlinx.android.synthetic.main.fragment_add_progress.*
import org.koin.android.viewmodel.ext.android.viewModel
import pl.kuben.progressapp.R
import pl.kuben.progressapp.add.AddConnector
import pl.kuben.progressapp.base.BaseFragment
import pl.kuben.progressapp.plain.NO_ID

class AddProgressFragment : BaseFragment<AddProgressViewModel>(), AddConnector {

    override val viewModel: AddProgressViewModel by viewModel()
    private var progressId: Int = NO_ID

    private var _isEdited = false
    override val isEdited: Boolean
        get() = _isEdited

    private var color: Int = Color.YELLOW
        set(value) {
            field = value
            colorButton.drawable.setTint(value)
        }

    private var secondaryColor: Int = Color.WHITE
        set(value) {
            field = value
            backgroundColorButton.drawable.setTint(value)
        }

    companion object {
        const val ARG_PROGRESS_ID = "progress_id"

        fun newInstance(progressId: Int = NO_ID): AddProgressFragment {
            return AddProgressFragment().apply {
                arguments = bundleOf(ARG_PROGRESS_ID to progressId)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_progress, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressId = arguments?.getInt(ARG_PROGRESS_ID) ?: NO_ID
        color = ContextCompat.getColor(activity!!, R.color.defaultProgressPrimaryColor)
        secondaryColor = ContextCompat.getColor(activity!!, R.color.defaultProgressSecondaryColor)
        setup()
    }

    private fun setup() {
        setupListeners()
        setupViews()
        maxNumberPicker.value = 10
        viewModel.saved.observe(this, Observer {
            if (it == true) {
                activity?.onBackPressed()
            } else {
                showSnackBar(getString(R.string.error))
            }
        })
        viewModel.progress.observe(this, Observer {
            if (it != null) {
                nameEditText.setText(it.name)
                color = it.color
                secondaryColor = it.backgroundColor
                maxNumberPicker.value = it.maxValue
                styleSpinner.setSelection(it.style)
            }
        })
        viewModel.getProgress(progressId)
    }

    private fun setupListeners() {
        addButton.setOnClickListener {
            _isEdited = false
            viewModel.addProgress(
                    nameEditText.text.toString(),
                    maxNumberPicker.value,
                    color,
                    secondaryColor,
                    styleSpinner.selectedItem as Int
            )
        }
        colorButton.setOnClickListener {
            openColorPicker(color) { color ->
                this.color = color
                _isEdited = true
            }
        }
        backgroundColorButton.setOnClickListener {
            openColorPicker(secondaryColor) { color ->
                secondaryColor = color
                _isEdited = true
            }
        }
        nameEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) = Unit
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                _isEdited = true
            }
        })
    }

    private fun setupViews() {
        color = ContextCompat.getColor(activity!!, R.color.blue)
        maxNumberPicker.minValue = 1
        maxNumberPicker.maxValue = Int.MAX_VALUE
        maxNumberPicker.wrapSelectorWheel = false
        styleSpinner.adapter = StyleSpinnerAdapter()
    }
}