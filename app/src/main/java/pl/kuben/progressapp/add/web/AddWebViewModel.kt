package pl.kuben.progressapp.add.web

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.support.annotation.ColorInt
import io.reactivex.Single
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import pl.kuben.progressapp.R
import pl.kuben.progressapp.base.BaseViewModel
import pl.kuben.progressapp.common.standardWatch
import pl.kuben.progressapp.data.Repository
import pl.kuben.progressapp.data.ResourceProvider
import pl.kuben.progressapp.data.model.db.ProgressCount
import pl.kuben.progressapp.data.model.db.WebEntity
import pl.kuben.progressapp.data.model.db.WebProgress
import timber.log.Timber
import java.util.concurrent.TimeUnit

private const val CLOSE_DELAY = 1L

class AddWebViewModel(resourceProvider: ResourceProvider, private val repository: Repository) : BaseViewModel(resourceProvider) {

    private val _progresses = MutableLiveData<List<ProgressCount>>()
    val progresses: LiveData<List<ProgressCount>> by lazy { _progresses }
    private val _shouldClose = MutableLiveData<Boolean>()
    val shouldClose: LiveData<Boolean> by lazy { _shouldClose }

    fun loadData() {
        disposables += repository.getProgressCounts()
                .standardWatch()
                .subscribeBy(
                        onSuccess = { _progresses.value = it },
                        onError = { Timber.e(it) }
                )
    }

    fun addWebProgress(name: String, @ColorInt color: Int, progressCounts: List<ProgressCount>) {
        if (progressCounts.size >= 3) {
            disposables += repository.insertWebProgress(WebProgress(name, color))
                    .observeOn(Schedulers.io())
                    .subscribeOn(Schedulers.io())
                    .flatMap { webProgressId ->
                        repository.insertWebEntities(progressCounts.map { WebEntity(it.id!!.toLong(), webProgressId) })
                    }
                    .standardWatch()
                    .subscribeBy(
                            onSuccess = {
                                successfullyAdded(name)
                            },
                            onError = {
                                Timber.e(it)
                                message = getString(R.string.error)
                            }
                    )
        } else {
            message = getString(R.string.too_little_progresses)
        }
    }

    private fun successfullyAdded(name: String) {
        message = getString(R.string.added_web_progress, name)
        disposables += Single.timer(CLOSE_DELAY, TimeUnit.SECONDS)
                .standardWatch()
                .subscribeBy { _shouldClose.value = true }
    }
}