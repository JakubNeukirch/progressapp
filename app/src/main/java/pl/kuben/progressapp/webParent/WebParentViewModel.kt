package pl.kuben.progressapp.webParent

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import pl.kuben.progressapp.base.BaseViewModel
import pl.kuben.progressapp.common.Settings
import pl.kuben.progressapp.common.standardWatch
import pl.kuben.progressapp.data.Repository
import pl.kuben.progressapp.data.ResourceProvider
import pl.kuben.progressapp.data.model.WebProgressEntity
import pl.kuben.progressapp.main.MainActivity
import timber.log.Timber

class WebParentViewModel(
        resourceProvider: ResourceProvider,
        private val repository: Repository,
        private val settings: Settings
)
    : BaseViewModel(resourceProvider) {

    private val _webProgresses = MutableLiveData<List<WebProgressEntity>>()
    val webProgresses: LiveData<List<WebProgressEntity>> by lazy { _webProgresses }

    fun onResume() {
        loadWebProgresses()
        settings.lastScreen = MainActivity.Screen.WEB.type
    }

    fun loadWebProgresses() {
        disposables += repository.getWebProgresses()
                .standardWatch()
                .subscribeBy(
                        onSuccess = {
                            _webProgresses.value = it.filter { webProgressEntity -> webProgressEntity.progresses?.size ?: 0 > 2 }
                        },
                        onError = {
                            Timber.e(it)
                        }
                )
    }
}