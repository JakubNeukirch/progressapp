package pl.kuben.progressapp.web

import android.support.v7.app.AlertDialog
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import kotlinx.android.synthetic.main.fragment_web.*
import org.koin.android.viewmodel.ext.android.viewModel
import pl.jakubneukirch.webchart.Point
import pl.kuben.progressapp.R
import pl.kuben.progressapp.base.BaseFragment
import pl.kuben.progressapp.data.model.db.ProgressCount
import pl.kuben.progressapp.webParent.WebParentFragment

class WebFragment : BaseFragment<WebViewModel>() {

    override val viewModel: WebViewModel by viewModel()
    private val webProgressId by lazy {
        arguments?.getLong(ARG_PROGRESS_ID)
    }
    private val confirmationDialog: AlertDialog by lazy {
        AlertDialog.Builder(context!!)
                .setTitle(R.string.confirm_remove_web_progress)
                .setPositiveButton(R.string.dialog_ok) { dialog, _ ->
                    viewModel.deleteWebProgress()
                    dialog.dismiss()
                }
                .setNegativeButton(R.string.dialog_cancel) { dialog, _ ->
                    dialog.dismiss()
                }
                .create()
    }

    companion object {
        private const val ARG_PROGRESS_ID = "progressID"

        fun newInstance(webProgressId: Long): WebFragment {
            return WebFragment().apply {
                arguments = bundleOf(ARG_PROGRESS_ID to webProgressId)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_web, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        setupListeners()
        subscribeToWebProgress()
        subscribeToShouldClose()
        viewModel.loadData(webProgressId!!)
    }

    private fun setupListeners() {
        deleteButton.setOnClickListener {
            showConfirmationDialog()
        }
    }

    private fun showConfirmationDialog() {
        confirmationDialog.show()
    }

    private fun subscribeToShouldClose() {
        viewModel.shouldClose.observe(this, Observer {
            if (it == true) {
                (parentFragment as WebParentFragment?)?.refresh()
            }
        })
    }

    private fun subscribeToWebProgress() {
        viewModel.webProgressEntity.observe(this, Observer { webProgressEntity ->
            webProgressEntity?.run {
                webChart.points = progresses!!.map { it.toPoint() }
                webChart.webColor = color
                webCardView.strokeColor = color
            }
        })
    }

    private fun ProgressCount.toPoint(): Point {
        return Point(this.count, this.maxValue, this.name)
    }
}