package pl.kuben.progressapp.web

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import pl.kuben.progressapp.R
import pl.kuben.progressapp.base.BaseViewModel
import pl.kuben.progressapp.common.standardWatch
import pl.kuben.progressapp.data.Repository
import pl.kuben.progressapp.data.ResourceProvider
import pl.kuben.progressapp.data.model.WebProgressEntity
import pl.kuben.progressapp.data.model.db.Progress
import pl.kuben.progressapp.data.model.db.WebProgress
import timber.log.Timber

class WebViewModel(resourceProvider: ResourceProvider, private val repository: Repository)
    : BaseViewModel(resourceProvider) {

    private var webProgressId: Long = -1L

    private val _webProgressEntities = MutableLiveData<WebProgressEntity>()
    val webProgressEntity: LiveData<WebProgressEntity> by lazy { _webProgressEntities }
    private val _webProgress = MutableLiveData<WebProgress>()
    val webProgress: LiveData<WebProgress> by lazy { _webProgress }
    private val _progresses = MutableLiveData<List<Progress>>()
    val progress: LiveData<List<Progress>> by lazy { _progresses }
    private val _shouldClose = MutableLiveData<Boolean>()
    val shouldClose: LiveData<Boolean> by lazy { _shouldClose }

    fun loadData(webProgressId: Long) {
        this.webProgressId = webProgressId
        disposables += repository.getWebProgress(webProgressId)
                .standardWatch()
                .subscribeBy(
                        onSuccess = {
                            Timber.d("$it")
                            _webProgressEntities.value = it
                        },
                        onError = { Timber.e(it) }
                )
    }

    fun deleteWebProgress() {
        disposables += repository.deleteWebProgress(webProgressId)
                .standardWatch()
                .subscribeBy(
                        onSuccess = {
                            message = getString(R.string.web_progress_removed)
                            _shouldClose.value = true
                        },
                        onError = {
                            message = getString(R.string.error)
                        }
                )
    }
}